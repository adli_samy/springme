package com.learning.firstspring.courses;

import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface CourseRepository extends  CrudRepository<Course,String> {
    public List<Course> findByTopicName(String topicId);
}
