package com.learning.firstspring.courses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CourseService {
    @Autowired
    private CourseRepository courseRepository;

    private List<Course> courses = new ArrayList(Arrays.asList(
            new Course("Spring","Spring Framework","Java Spring Framework"),
                new Course("Angular","Angular Framework","Angular Framework")
        )
    );


    public List<Course> getCourses(String topicId) {
        List <Course> courses = new ArrayList<Course>();
        courseRepository.findByTopicName(topicId).forEach(courses::add);
        return courses;
    }
    public Course getCourse(String id){
        return  courseRepository.findById(id).get();
        //return  topics.stream().filter(topic -> topic.getName().equals(id)).findFirst().get();
     }
     public void addTopic(Course course){
         courseRepository.save(course);
        //topics.add(topic);
     }
    public void updateCourse(Course course){
//        topics.forEach(topic1 -> {
//            if(topic1.getName().equals(id)){
//                   topics.set(topics.indexOf(topic1),topic);
//                   return;
//            }
//        });
        courseRepository.save(course);
    }
    public void deleteTopic(String id){
//        topics.removeIf(topic -> topic.getName().equals(id));
        courseRepository.deleteById(id);
    }
}
