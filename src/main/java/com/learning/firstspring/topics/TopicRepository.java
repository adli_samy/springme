package com.learning.firstspring.topics;

import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends  CrudRepository<Topic,String> {

}
