package com.learning.firstspring.topics;

import com.learning.firstspring.topics.Topic;
import com.learning.firstspring.topics.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService {
    @Autowired
    private TopicRepository topicRepository;

    private List<Topic> topics = new ArrayList(Arrays.asList(
            new Topic("Spring","Spring Framework","Java Spring Framework"),
                new Topic("Angular","Angular Framework","Angular Framework")
        )
    );


    public List<Topic> getTopics() {
        List <Topic> topics = new ArrayList<Topic>();
        topicRepository.findAll().forEach(topics::add);
        return topics;
    }
    public Topic getTopic(String id){
        return  topicRepository.findById(id).get();
        //return  topics.stream().filter(topic -> topic.getName().equals(id)).findFirst().get();
     }
     public void addTopic(Topic topic){
         topicRepository.save(topic);
        //topics.add(topic);
     }
    public void updateTopic(Topic topic,String id){
//        topics.forEach(topic1 -> {
//            if(topic1.getName().equals(id)){
//                   topics.set(topics.indexOf(topic1),topic);
//                   return;
//            }
//        });
        topicRepository.save(topic);
    }
    public void deleteTopic(String id){
//        topics.removeIf(topic -> topic.getName().equals(id));
        topicRepository.deleteById(id);
    }
}
